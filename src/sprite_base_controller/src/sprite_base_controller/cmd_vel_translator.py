#!/usr/bin/env python3

import rospy

from ackermann_msgs.msg import AckermannDrive
from geometry_msgs.msg import Twist


def handle_cmd_vel(data):
    global ackermann_cmd_pub

    # Assuming TEB local planner, and cmd_angle_instead_rotvel=true
    v = data.linear.x
    steering = data.angular.z

    msg = AckermannDrive()
    msg.steering_angle = steering
    msg.speed = v

    ackermann_cmd_pub.publish(msg)


def main():
    global ackermann_cmd_pub
    rospy.init_node("cmd_vel_translator")

    rospy.Subscriber("/cmd_vel", Twist, handle_cmd_vel, queue_size=1)
    ackermann_cmd_pub = rospy.Publisher("/ackermann_cmd", AckermannDrive, queue_size=1)

    rospy.spin()


if __name__ == "__main__":
    main()
