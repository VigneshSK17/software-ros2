#!/usr/bin/env python3

import rospy
import tf2_ros

from gazebo_msgs.msg import ModelStates
from geometry_msgs.msg import TransformStamped
from nav_msgs.msg import Odometry


def handle_model_states(msg):
    global odom_broadcaster, odom_pub
    current_time = rospy.Time.now()

    try:
        car_pos = msg.name.index("ackermann_vehicle")
    except ValueError:
        return

    odom_trans = TransformStamped()
    odom_trans.header.stamp = current_time
    odom_trans.header.frame_id = "odom"
    odom_trans.child_frame_id = "base_link"

    odom_trans.transform.translation = msg.pose[car_pos].position
    odom_trans.transform.rotation = msg.pose[car_pos].orientation

    odom_broadcaster.sendTransform(odom_trans)

    odom = Odometry()
    odom.header.stamp = current_time
    odom.header.frame_id = "odom"
    odom.child_frame_id = "base_link"

    odom.pose.pose = msg.pose[car_pos]
    odom.twist.twist = msg.twist[car_pos]

    odom_pub.publish(odom)


def main():
    global odom_broadcaster, odom_pub
    rospy.init_node("sprite_base_controller")

    rospy.Subscriber("/gazebo/model_states", ModelStates, handle_model_states)
    odom_pub = rospy.Publisher("odom", Odometry, queue_size=1)
    odom_broadcaster = tf2_ros.TransformBroadcaster()

    rospy.spin()


if __name__ == "__main__":
    main()
