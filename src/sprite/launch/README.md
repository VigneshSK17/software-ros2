This directory contains the main launch files for Project Sprite.

## First-level Launch Files

These launch files are meant to be launched using `roslaunch` (e.g. `roslaunch sprite xxxxxxx.launch`).

- [`main.launch`](main.launch): This is the main launch file used to start the project on a physical vehicle. It
  includes [`actual_vehicle.launch`](actual_vehicle.launch) (described below). It also includes a launch file from the
  [`sprite_base_controller`](../../sprite_base_controller) package to start the base controller for the physical
  vehicle. Finally, it also includes the launch file from the [`sprite_navigation`](../../sprite_navigation) package to
  start the navigation stack.

- [`simulation.launch`](simulation.launch): This is the main launch file used to run the project on a vehicle in a
  simulated environment. Its structure is parallel to that of [`main.launch`](main.launch), using
  [`simulated_vehicle.launch`](simulated_vehicle.launch) (described below) in place of
  [`actual_vehicle.launch`](actual_vehicle.launch), and including a launch file from the
  [`sprite_base_controller`](../../sprite_base_controller) package for the simulated vehicle instead of the physical
  vehicle.

- [`rviz.launch`](rviz.launch): This launch file starts [RViz](https://wiki.ros.org/rviz), a tool useful for visualizing
  data flowing through the different ROS topics. It is loaded with a default configuration that visualizes data from the
  LiDAR sensor, as well as various data from the navigation stack.

- [`gzclient.launch`](gzclient.launch): This launch file starts the [Gazebo](http://gazebosim.org/) simulation frontend.
  It is useful for visualizing the 3D world simulated by the Gazebo backend (when running
  [`simulation.launch`](simulation.launch)).

- [`simulation_gui.launch`](simulation_gui.launch): This launch file is a simple shortcut to launch the simulation along
  with RViz and the Gazebo frontend. It simply includes [`simulation.launch`](simulation.launch),
  [`rviz.launch`](rviz.launch), and [`gzclient.launch`](gzclient.launch).

## Other Launch Files

These launch files are not meant to be launched directly with `roslaunch`, but rather are included from other launch
files.

- [`actual_vehicle.launch`](actual_vehicle.launch): This launch file handles setting everything up related to the
  physical vehicle (as of now, this includes defining static transforms, and starting the scanning LiDAR sensor driver
  and the IMU driver)

- [`simulated_vehicle.launch`](simulated_vehicle.launch): This launch file is a parallel to
  [`actual_vehicle.launch`](actual_vehicle.launch), in that it sets up everything related to the simulated vehicle. This
  includes:

  - Loading the vehicle model using [`simulated_vehicle_urdf.launch`](simulated_vehicle_urdf.launch) (described below).

  - Starting the Gazebo simulation with a specific world (from [src/sprite/worlds/](../worlds/)).

  - Spawning the vehicle into the simulated world, and setting up the necessary physics controllers.

- [`simulated_vehicle_urdf.launch`](simulated_vehicle_urdf.launch): This launch file loads in the vehicle model from the
  URDF file (located at [src/sprite/urdf/traxxas_6804r.urdf.xacro](../urdf/traxxas_6804r.urdf.xacro)).
