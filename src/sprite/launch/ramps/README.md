This directory contains the launch files for the ramp simulations.

## First-level Launch Files

These launch files are meant to be launched using `roslaunch` (e.g. `roslaunch sprite xxxxxxx.launch`).

- [`ramp_simulation.launch`](ramp_simulation.launch): This is the launch file used to run the project on a vehicle
  in a simulated environment with different ramps.
- [`ramp_gui.launch`](ramp_gui.launch): This launch file is a simple shortcut to launch the simulation along
  with RViz and the Gazebo frontend. It simply includes [`ramp_simulation.launch`](ramp_simulation.launch),
  [`rviz.launch`](src/sprite/launch/rviz.launch), and [`gzclient.launch`](src/sprite/launch/gzclient.launch).

## Other Launch Files

These launch files are not meant to be launched directly with `roslaunch`, but rather are included from other launch
files.

- [`ramp.launch`](ramp.launch): This launch file is a parallel to [`simulated_vehicle.launch`](src/sprite/launch/simulated_vehicle.launch)
  and [`actual_vehicle.launch`](src/sprite/launch/actual_vehicle.launch), in that it sets up everything related to the simulated vehicle. This
  includes:

  - Loading the vehicle model using [`noisy_vehicle_urdf.launch`](noisy_vehicle_urdf.launch) (described below).

  - Starting the Gazebo simulation with a specific world (from [src/sprite/worlds/](src/sprite/worlds/)).

  - Spawning the vehicle into the simulated world, and setting up the necessary physics controllers.
