#!/usr/bin/env python3

loginfo = print

try:
    import rosgraph
    import rospy

    if rosgraph.is_master_online():
        rospy.init_node("sprite_telemetry")
        loginfo = rospy.loginfo
except ImportError:
    pass

from serial import Serial
from serial.tools.list_ports import comports
import sys

serial_ports = comports()

# Base Station: IN is for GPS, OUT is for XBee (vice versa for Vehicle)
# PORT: path on the linux system that is used to talk to the device
# BAUD: speed at which communication happens (agreed on by both sides)
IN_PORT = ''
IN_BAUD = 0
OUT_PORT = ''
OUT_BAUD = 0

try:
    IN_PORT = sys.argv[1]
    print()
    loginfo("Input port: {}".format(IN_PORT))
    IN_BAUD = int(sys.argv[2])
    loginfo("Input baud rate: {}".format(IN_BAUD))
    OUT_PORT = sys.argv[3]
    loginfo("Output port: {}".format(OUT_PORT))
    OUT_BAUD = int(sys.argv[4])
    loginfo("Output baud rate: {}".format(OUT_BAUD))

except (IndexError, ValueError):
    while not IN_PORT:
        serial_ports = comports()
        print("\nSelect input port:")
        for i, serial_port in enumerate(serial_ports):
            print("%i) %s" % (i + 1, serial_port.device))
        try:
            choice = int(input(": ")) - 1
            if choice < 0:
                raise IndexError
            IN_PORT = serial_ports[choice].device
        except (NameError, IndexError, ValueError):
            print("Invalid choice, try again.")

    while not IN_BAUD:
        try:
            IN_BAUD = int(input("Input baud rate: "))
        except ValueError:
            print("Invalid number, try again.")

    while not OUT_PORT:
        serial_ports = comports()
        print("\nSelect output port:")
        for i, serial_port in enumerate(serial_ports):
            print("%i) %s" % (i + 1, serial_port.device))
        try:
            choice = int(input(": ")) - 1
            if choice < 0:
                raise IndexError
            OUT_PORT = serial_ports[choice].device
        except (NameError, IndexError, ValueError):
            print("Invalid choice, try again.")

    while not OUT_BAUD:
        try:
            OUT_BAUD = int(input("Output baud rate: "))
        except ValueError:
            print("Invalid number, try again.")

print()
loginfo("Starting bridge:")
loginfo(" Input: %s @ %i baud" % (IN_PORT, IN_BAUD))
loginfo(" Output: %s @ %i baud" % (OUT_PORT, OUT_BAUD))
print()
print("Press Ctrl-C to exit.")

with Serial(IN_PORT, IN_BAUD) as in_ser, Serial(OUT_PORT, OUT_BAUD) as out_ser:
    while True:
        data = in_ser.read()
        out_ser.write(data)
        # base station: reads from Reach RS2, outputs to XBee
        # actual vehicle: reads from XBee, outputs to Reach M2
        sys.stdout.write('.')
        sys.stdout.flush()
