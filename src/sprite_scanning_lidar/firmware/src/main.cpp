#include <Arduino.h>
#include <Servo.h>

/*
 * Sprite Scanning LiDAR
 */

#define PIN_SCAN_RGF_SYN  2

#define PIN_SCAN_ENC_IDX  7
#define PIN_SCAN_ENC_CHA  8

#define PIN_SCAN_ESC_PWM  9

#define SCAN_ESC_PWM_SET  1748



// Scanning mechanism rangefinder variables
byte Byte_L, Byte_H;
double scan_dist;

volatile unsigned long scan_time_vol;

unsigned long scan_time;
short scan_count;
unsigned long scan_count_time;

// Scanning mechanism encoder variables
volatile short scan_enc_count_vol;
volatile unsigned long scan_enc_count_time_vol;

// Scanning mechanism ESC variables
Servo scan_esc;

// String format variables
char dist_temp[8];
char time_temp[16];
char count_temp[4];
char ctime_temp[16];
char ray_temp[32];


/*
  Interrupt Service Routines
*/

// Store upcoming rangefinder reading's time and encoder details
void isr_scan_rgf_syn() {
  scan_time_vol = micros();
}

// Reset count on index pulse
void isr_scan_enc_idx() {
  scan_enc_count_time_vol = micros();
  scan_enc_count_vol = 0;
}

// Increment count on channel A pulse
void isr_scan_enc_cha() {
  scan_enc_count_time_vol = micros();
  ++scan_enc_count_vol;
}



/*
  Initialization Functions
*/

// Sets up the scanning mechanism rangefinder
void init_scan_rgf() {
  pinMode(PIN_SCAN_RGF_SYN, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_SCAN_RGF_SYN), isr_scan_rgf_syn, FALLING);
}

// Sets up the scanning mechanism encoder
void init_scan_enc() {
  pinMode(PIN_SCAN_ENC_IDX, INPUT);
  pinMode(PIN_SCAN_ENC_CHA, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_SCAN_ENC_IDX), isr_scan_enc_idx, RISING);
  attachInterrupt(digitalPinToInterrupt(PIN_SCAN_ENC_CHA), isr_scan_enc_cha, RISING);
}

// Sets up the scanning mechanism ESC
void init_scan_esc() {
  scan_esc.attach(PIN_SCAN_ESC_PWM);
  scan_esc.writeMicroseconds(SCAN_ESC_PWM_SET);
}



/*
 * Update Functions
 */

void update_scan() {
  if (Serial1.available() >= 2) {
    // Read raw data
    Byte_H = Serial1.read();
    Byte_L = Serial1.read();

    // Calculate distance
    scan_dist =  Byte_L / 256.0 + Byte_H;

    // Save volatile variables
    noInterrupts();
    scan_time = scan_time_vol;
    scan_count = scan_enc_count_vol;
    scan_count_time = scan_enc_count_time_vol;
    interrupts();

    // Transmit ray details
    // Format: <distance>:<time>:<previous angle>:<time of previous angle>
    dtostrf(scan_dist, 0, 2, dist_temp);
    dtostrf(scan_time, 0, 0, time_temp);
    dtostrf(scan_count, 0, 0, count_temp);
    dtostrf(scan_count_time, 0, 0, ctime_temp);
    sprintf(ray_temp, "%s,%s,%s,%s", dist_temp, time_temp, count_temp, ctime_temp);
    Serial.println(ray_temp);
  }
}



/*
  Default Functions
*/

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);

  Serial1.begin(921600);
  Serial.begin(3500000);  // USB serial, for communication with host
  while (!Serial) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
    delay(500);
  };                      // Wait for host to connect to port
  digitalWrite(LED_BUILTIN, LOW);

  init_scan_esc();
  init_scan_enc();
  init_scan_rgf();
}

void loop() {
  update_scan();
}