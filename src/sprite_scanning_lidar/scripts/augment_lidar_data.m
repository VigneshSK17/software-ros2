function [lidar_data] = augment_lidar_data(lidar_data)

for i = 1:height(lidar_data)

    if (lidar_data.count(i) == 0 && lidar_data.count(i-1) == 200)
        lidar_data.count(i) = lidar_data.count(i - 1);
        lidar_data.ctime(i) = lidar_data.ctime(i - 1);
    end
    
    ct_delta = 0;
    if i < height(lidar_data)
        ct_delta = lidar_data.ctime(i + 1) - lidar_data.ctime(i);
    end
    if ct_delta == 0
        ct_delta = lidar_data.ctime(i) - lidar_data.ctime(i - 1);
    end
    
    time_offset = lidar_data.time(i) - lidar_data.ctime(i);
    adj_count = mod(lidar_data.count(i) + time_offset / ct_delta, 200);
    lidar_data.angle(i) = 2 * pi * adj_count / 200;

end

end

