#!/usr/bin/env bash

source scripts/utils.sh --prefix "$0"
set -e
set -o pipefail

update_apt_cache
apt_install python3-pip
update_pip_cache

prefix_run scripts/install/cmake_lint_tools.sh
prefix_run scripts/install/python_lint_tools.sh
prefix_run scripts/install/other_lint_tools.sh

prefix_print All done.
