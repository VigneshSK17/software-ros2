#!/usr/bin/env bash

source scripts/utils.sh --prefix "$0"

apt_install curl

if [ ! -f "/etc/apt/sources.list.d/nodesource.list" ]; then
  prefix_print "Adding NodeSource LTS repository..."
  curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
  export APT_CACHE_UPDATED=1
fi
apt_install nodejs

if [ ! -f "/etc/apt/sources.list.d/yarn.list" ]; then
  prefix_print "Adding Yarn repository..."
  curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
  unset APT_CACHE_UPDATED
fi
apt_install yarn

PRETTIER_PACKAGE="prettier@2.8.3"

INSTALL_STATE=$(yarn global list | grep -c "$PRETTIER_PACKAGE" || true) # 0 == prettier is not installed
if [ "$INSTALL_STATE" == "0" ]; then
  prefix_run yarn global add $PRETTIER_PACKAGE
fi

which prettier > /dev/null
if [ "$?" != 0 ]; then
  prefix_print "Adding the global yarn bin to the PATH..."
  echo "export PATH=\$PATH:\`yarn global bin --offline\`" >> ~/.$(basename $SHELL)rc
  echo -e "\nDone. Make sure to close this terminal and open a new one before continuing."
  echo -e "Alternatively, run \"source ~/.$(basename $SHELL)rc\" before continuing."
fi
